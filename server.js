import express from 'express'
import hogan from 'hogan-express'
//import http_module from 'http'

const app = express()

app.engine('html', hogan)

var port = process.env.PORT || 5000;
app.set('port', port)

app.use(express.static('dist'));
app.use(express.static('bower_components'));

var Cosmic = require('cosmicjs')
var bucket_slug = process.env.COSMIC_BUCKET || 'white-dental'

var partials = {
    header: 'partials/header',
    navigation: 'partials/navigation',
    footer: 'partials/footer'
}
require('./routes')(app, partials)

//const http = http_module.Server(app)
app.listen(app.get('port'))
//http.listen(app.get('port'), () => {
  //  console.info('==> 🌎  Go to http://localhost:%s', app.get('port'));
//})
