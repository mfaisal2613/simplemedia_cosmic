import Cosmic from 'cosmicjs'

module.exports = (app, partials) =>{
    app.get('/contact', function (req, res) {
        Cosmic.getObjects({ bucket: { slug: 'simplemedia' } }, (err, response) => {
            res.locals.cosmic = response
            return res.render('contact.html', {
                partials,
            })
        })
    })
}