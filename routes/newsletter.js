import API from 'fetch-api';

module.exports = (app, partials) =>{
    var bodyParser = require('body-parser');
    app.use(bodyParser.json()); // support json encoded bodies
    app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

    app.post('/newsletter', function (req, res) {
        console.log('in the route');
        let name = req.body.name.split(' ');
        let addContactBody = {
            "properties": [
                {
                    "property": "email",
                    "value": req.body.email
                },
                {
                    "property": "firstname",
                    "value": name[0]
                },
            ]
        }

        if( name.length > 1 ) {
            addContactBody.properties.push({
                "property": "lastname",
                "value": name[1]
            });
        }

        let api = new API({
            baseURI: 'https://api.hubapi.com'
        });

        console.log('starting the api call');
        api.post(
        '/contacts/v1/contact?hapikey=01d6e8ff-cd27-4e8f-87bb-d3e7c81f2318',
            { body: JSON.stringify(addContactBody) },
            (err, res1, message) => {
                console.log('done with the first api call');
                let status = 0;
                if( res1.status == 200 ) {
                    let addToListBody = {
                        "vids": [ message.vid ]
                    };

                    api.post(
                        '/contacts/v1/lists/36/add?hapikey=01d6e8ff-cd27-4e8f-87bb-d3e7c81f2318',
                        { body: JSON.stringify(addToListBody) },
                        (err, res2, message) => {
                            console.log('done with the second api call');
                            /*if(res2.status == 200){
                                res.status(200).send(1);
                            } else {
                                //res.status(200).send('There was and error. Please try again later');
                            }*/

                            res.send('abcd');
                        }
                    );
                } else if (res1.status == '409') {
                    //res.status(200).send(2);
                    status = 1;
                }
            }
        );
    })
}