import Cosmic from 'cosmicjs'

module.exports = (app, partials) =>{
    app.get('/privacy-policy', function (req, res) {
        Cosmic.getObjects({ bucket: { slug: 'simplemedia' } }, (err, response) => {
            res.locals.cosmic = response
            return res.render('privacy-policy.html', {
                partials,
                color: 'light'
            })
        })
    })
}