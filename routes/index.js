
module.exports = (app, partials) => {
    require('./home')(app, partials)
    require('./work')(app, partials)
    require('./services')(app, partials)
    require('./process')(app, partials)
    require('./about')(app, partials)
    require('./blog')(app, partials)
    require('./contact')(app, partials)
    require('./terms')(app, partials)
    require('./privacy-policy')(app, partials)
    require('./newsletter')(app, partials)
}