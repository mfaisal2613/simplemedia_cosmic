import Cosmic from 'cosmicjs'

module.exports = (app, partials) =>{
    app.get('/about', function (req, res) {
        Cosmic.getObjects({ bucket: { slug: 'simplemedia' } }, (err, response) => {
            res.locals.cosmic = response
            partials.brands = 'partials/brands';
            return res.render('about.html', {
                partials,
            })
        })
    })
}