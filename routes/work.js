import Cosmic from 'cosmicjs'
module.exports = (app, partials) =>{

    app.get('/works/:slug', (req, res) => {
        const slug = req.params.slug
        Cosmic.getObjects({ bucket: { slug: 'simplemedia' } }, (err, response) => {
            res.locals.cosmic = response
            const works = response.objects.type.works
            works.forEach(page => {
                if (page.slug === slug) {
                    res.locals.page = page
                }
            })
            if (!res.locals.page) {
                return res.status(404).render('404.html', {
                    partials
                })
            }
            return res.render('work.html', {
                partials
            })
        })
    })
}