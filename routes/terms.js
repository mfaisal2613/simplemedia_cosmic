import Cosmic from 'cosmicjs'

module.exports = (app, partials) =>{
    app.get('/terms', function (req, res) {
        Cosmic.getObjects({ bucket: { slug: 'simplemedia' } }, (err, response) => {
            res.locals.cosmic = response
            return res.render('terms.html', {
                partials,
                color: 'light'
            })
        })
    })
}