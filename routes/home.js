import Cosmic from 'cosmicjs'

module.exports = (app, partials) =>{
    app.get('/', function (req, res) {
        Cosmic.getObjects({ bucket: { slug: 'simplemedia' } }, (err, response) => {
            res.locals.cosmic = response
            var works = response.objects.type.works
            res.locals.works = works
            works.forEach((item, i) => {
                item.work_class = (item.metafield.grid_thumbnail_size.value == 'aspect-ratio-2-1') ? 'image-grid__item--full' : ''
            })
            partials.brands = 'partials/brands';
            return res.render('index.html', {
                partials
            })
        })
    })
}