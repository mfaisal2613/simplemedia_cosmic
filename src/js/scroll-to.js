(function($) {

	// cache DOM
	var $bannerScroll = $('.banner__scroll');

	// bind events
	$bannerScroll.on('click', bannerScrollClickHandler);

	/**
	 * [bannerScrollClickHandler]
	 * @return {[void]}
	 */
	function bannerScrollClickHandler() {
		$('html, body').animate({
			scrollTop: $('.scroll-target').offset().top
		}, 1500);
	}

})(jQuery);