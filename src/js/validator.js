(function($){
	var opts = {
	  lines: 13, // The number of lines to draw
	  length: 17, // The length of each line
	  width: 5, // The line thickness
	  radius: 35, // The radius of the inner circle
	  scale: 0.5, // Scales overall size of the spinner
	  corners: 1, // Corner roundness (0..1)
	  color: '#fff', // #rgb or #rrggbb or array of colors
	  opacity: 0.2, // Opacity of the lines
	  rotate: 12, // The rotation offset
	  direction: 1, // 1: clockwise, -1: counterclockwise
	  speed: 1.4, // Rounds per second
	  trail: 44, // Afterglow percentage
	  fps: 20, // Frames per second when using setTimeout() as a fallback for CSS
	  zIndex: 2e9, // The z-index (defaults to 2000000000)
	  className: 'spinner', // The CSS class to assign to the spinner
	  top: '50%', // Top position relative to parent
	  left: '50%', // Left position relative to parent
	  shadow: false, // Whether to render a shadow
	  hwaccel: false, // Whether to use hardware acceleration
	  position: 'absolute' // Element positioning
	};
	var target = document.getElementById('spinner');
	var spinner = new Spinner(opts).spin();
	target.appendChild(spinner.el);
	
    var form = $('#newsletter-form');
    var formMessages = $('#form-messages');	
	$(form).validate({
		 errorElement: 'div',
		 errorClass   : "font-c-danger",
        rules: {
            name: "required",
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            name: "Please enter your name",
            email: "Please enter a valid email address"
        },
        submitHandler: function(form) {		
				$(target).show();
				$.ajax({
					type: form.method,
					url: form.action,
					data: $(form).serialize()
				}).done(function(response) {
					$(target).hide();
					$(formMessages).show().addClass('alert');
					console.log(response);
					if(response == 1) {
						$(formMessages).html('<div class="success">Subscription Successful </div>');
						setTimeout(function(){$(form).fadeOut(1000);}, 2000);	
						$('#name').val('');
						$('#email').val('');
					} else if (response == 2) {
						$(formMessages).html('<div class="error">Contact already exists with this email</div>');
						setTimeout(function(){$(formMessages).fadeOut(1000);}, 10000);
					} else {
						$(formMessages).html('<div class="error">Oops! An error occured and your message could not be sent.</div>');
						setTimeout(function(){$(formMessages).fadeOut(1000);}, 10000);
					}
				}).fail(function(data) {
					$(target).hide();
					$(formMessages).show().addClass('alert');
					if (data.responseText !== '') {
						$(formMessages).html(data.responseText);
					} else {
						$(formMessages).html('<div class="error">Oops! An error occured and your message could not be sent.</div>');
					}
					setTimeout(function(){$(formMessages).fadeOut(1000);}, 10000);
				});
        }
    });
	
	var form2 = $('#contact-form');
	$(form2).validate({
		 errorElement: 'div',
		 errorClass   : "font-c-danger",
		 ignore: ':hidden:not(:checkbox)',
		 errorPlacement: function (error, element) {
			  if (element.is(":checkbox")) {
				  $('.checkbox-label').append(error);
			  } else {
				  error.insertAfter(element);
			  }
		 },
        rules: {
            fname: "required",
            lname: "required",
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                digits: true
            },
            'service[]': {
                required: true,
            },
			projectDetails: "required",
        },
        messages: {
            fname: "Please enter your first name",
            lname: "Please enter your last name",
            phone: "Please enter a valid phone number",
            email: "Please enter a valid email address",
			'service[]': {
                required: "You must select atleast one service"
            },
			
			projectDetails: "Please enter your project details",
        },
        submitHandler: function(form) {
			form2.submit();
		}
    });
	$('#contact-form').delegate('.checkbox', 'click', function(){
		if($(this).children('span.checked')) {
			$('.checkbox-label').html('');
		}
	});
})(jQuery);