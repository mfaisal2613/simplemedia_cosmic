(function($) {

	// cache DOM
	var $addOptions = $('.js-additional-options');
	var $orderTotal = $('.js-order-total');
	var $checkbox = $('.checkbox-two, .checkbox-one');

	// add more pages
	var $addMorePageContainer = $('.add-more-page');
	var $addMorePage = $('.add-more-page__item');
	var $addMoreBtn = $('.add-more-page__button');
	
	// globals
	var additionalOptions = {};
	var defaultPrice = $($orderTotal).data('default-price');

	var delay = (function() {
		var timer = 0;
		return function(callback, ms) {
			clearTimeout(timer);
			timer = setTimeout(callback, ms);
		};
	})();

	init();
	bindEvents();
	render();

	/**
	 * [render]
	 * @return {void}
	 */
	function render() {
		renderAddOptions();
		renderPrice();
	}

	/**
	 * [renderAddOptions]
	 * @return {void}
	 */
	function renderAddOptions() {
		var temp = '';

		// remove and render again
		$addOptions.children().remove();

		for(var prop in additionalOptions) {
			temp += createItem(prop, additionalOptions[prop]);
		}

		// append to additional options ui
		$addOptions.append(temp);
	}

	/**
	 * [renderPrice]
	 * @return {void}
	 */
	function renderPrice() {
		var tempPrice = 0;

		for(var prop in additionalOptions) {
			tempPrice += parseInt(additionalOptions[prop]);
		}

		tempPrice += parseInt(defaultPrice);
		$orderTotal.text(tempPrice);
	}

	/**
	 * [createItem]
	 * @param  {string} name
	 * @param  {string} price 
	 * @return {dom}
	 */
	function createItem(name, price) {
		if (typeof name !== 'string' && typeof price !== 'string') {
			return;
		}

		var div = '<div>';
			div += '<span>' + name + '</span>';
			div += '<span class="font-c-teal1"> $' + price + '</span>';
			div += '</div>';

		return div;
	}

	/**
	 * [init]
	 * @return {void}
	 */
	function init() {
		// default state of the UI
		
		// get default total price and assign to global var totalPrice
		if ($orderTotal.length) {
			totalPrice = parseInt($orderTotal.text());
		}

		// get all active checkbox value and assign to global var additionalOptions
		if ($checkbox.length) {
			$actives = $('.checkbox-two.active, .checkbox-one:checked');

			$actives.map(function(i, v) {
				var name = $(v).find('input').data('display-name');
				var value = $(v).find('input').val();

				if ($('.checkbox-one').length && $('.js-checbox-seo').length) {
					name = $(v).data('display-name');
					value = $(v).val();
				}

				additionalOptions[name] = value;
			});
		}

		// add more page
		if ($addMorePage.length) {
			$addMorePage.map(function(i, v) {
				var pagePrice = $(v).find('input').data('price');
				var pageName = $(v).find('input').data('page-name');
				additionalOptions[pageName] = pagePrice;
			});
		}
	}

	/**
	 * [bindEvents]
	 * @return {void}
	 */
	function bindEvents() {
		// bind events
		$checkbox.on('click', onCheckboxClick);
		$addMoreBtn.on('click', onAddMoreButtonClick);
		$addMorePageContainer.on('click', '.add-more-page__minus', onAddMoreMinusClick);
		$addMorePageContainer.on('keyup', '.add-more-page__txt.item', onAddMoreTextKeyup);
		$('.js-checbox-seo').on('click', '.checkbox', onCheckboxOneClick);
	}

	/**
	 * [onCheckboxOneClick]
	 * @return {void}
	 */
	function onCheckboxOneClick() {
		var $checkbox = $(this).parent().find('input');
		var name = $checkbox.data('display-name');
		var price = $checkbox.val();

		if ($checkbox && $checkbox.attr('checked')) {
			addItem(name, price);
		} else {
			removeItem(name);
		}

		render();
	}

	/**
	 * [onCheckboxClick]
	 * @return {void}
	 */
	function onCheckboxClick() {
		// perform add item on summary UI
		var $this = $(this);
		var $input = $this.find('input');
		var name = $input.data('display-name');
		var price = $input.val();

		if ($this.hasClass('active')) {
			addItem(name, price);
		} else {
			removeItem(name);
		}
		
		render();
	}

	/**
	 * [onAddMoreMinusClick]
	 * @return {void}
	 */
	function onAddMoreMinusClick() {
		var pageName = $(this).parent().find('input').data('page-name');
		removeItem(pageName);
	}

	/**
	 * [onAddMoreButtonClick]
	 * @return {void}
	 */
	function onAddMoreButtonClick() {
		var $input = $('.add-more-page__txt');

		if ($input.length) {
			var name = $input.eq(-1).data('page-name');
			var price = $input.eq(-1).data('price');

			addItem(name, price);
			render();
		}
	}

	/**
	 * [onAddMoreTextKeyup]
	 * @return {void}
	 */
	function onAddMoreTextKeyup() {
		delay(function() {
			removeItem($(this).attr('data-page-name'));
			
			// find key on additionalOptions object
			// and replace with new inputted name
			var newName = $(this).val();
			var price = $(this).data('price');

			$(this).attr('data-page-name', newName);
			addItem(newName, price);
			render();
		}.bind(this), 1000);
	}

	/**
	 * [removeItem]
	 * @param  {string} name
	 * @return {void}
	 */
	function removeItem(name) {
		if (typeof name === 'string') {
			delete additionalOptions[name];
			render();
		}
	}

	/**
	 * [addItem]
	 * @param {string} name
	 * @param {string} price
	 */
	function addItem(name, price) {
		if (typeof name !== 'string' && typeof price !== 'string') {
			return;
		}

		additionalOptions[name] = price;
	}

})(jQuery);