(function($) {

	'use strict';

	// cache DOM
	var $slickTestimonial = $('.slick-testimonial.js');

	render();

	/**
	 * [render]
	 * @return {void}
	 */
	function render() {
		// testimonial slick
		$slickTestimonial.slick({
			arrows: false,
			autoplay: true,
			autoplaySpeed: 6000,
			adaptiveHeight: true
		});
	}

})(jQuery);