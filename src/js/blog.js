(function($) {

	// cache DOM
	var $blog = $('.blog.js');

	render();

	/**
	 * [render]
	 * @return {void}
	 */
	function render() {
		var $masonry = $blog.isotope({
			itemSelector: '.blog__item',
			percentPosition: true,
			masonry: {
				columnWidth: '.blog__sizer'
			}
		});

		$masonry.imagesLoaded().progress(function () {
			$masonry.isotope('layout');
		});
	}

})(jQuery);