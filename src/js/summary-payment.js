(function($) {

	// cache DOM
	var $summaryTerm = $('.js-summary-term');
	var $radio = $('.radio');

	// globals
	var display = '';

	init();
	render();
	bindEvents();

	/**
	 * [render]
	 * @return {void}
	 */
	function render() {
		if ($summaryTerm.length) {
			$summaryTerm.text(display);
		}
	}

	function init() {
		if ($('.radio.active').length) {
			display = $('.radio.active').find('input').data('display');	
		}

		render();
	}

	/**
	 * [bindEvents]
	 * @return {void}
	 */
	function bindEvents() {
		$radio.on('click', radioClick);
	}

	/**
	 * [radioClick]
	 * @return {void}
	 */
	function radioClick() {
		display = $(this).find('input').data('display');
		render();
	}

})(jQuery);