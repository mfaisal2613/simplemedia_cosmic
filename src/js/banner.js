(function($) {

	// cache DOM
	$typed = $('.js-typed');

	init();

	/**
	 * [init]
	 * @return {void}
	 */
	function init() {
		$typed.typed({
			strings: ['strategy', 'brand', 'technology', 'ROI', 'success'],
			backDelay: 2500,
			typeSpeed: 100,
			loop: true
		});
	}

})(jQuery);