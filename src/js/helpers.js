(function ( $ ) {

	// cache dom
	var $illustration = $('.illustration.js');

	// bind events
	$illustration.mouseenter(function() {
		staggeredClass($(this).find('g'));
	}).mouseleave(function() {
		staggeredClass($(this).find('g'));
	});

	/**
	 * [staggeredClass]
	 * @param  {array} ar
	 * @return {void}
	 */
	function staggeredClass(ar) {
		var delay = 0;

		if (ar.length) {
			$.each(ar, function(i, v) {
				setTimeout(function() {
					if (!$(v).hasClass('label')) {
						if ($(v).hasClass('pulse')) {
							$(v).removeClass('pulse');	
						} else {
							$(v).addClass('pulse');
						}
					}
				}, delay);

				delay += 100;
			} );
		}
	}

} )( jQuery );